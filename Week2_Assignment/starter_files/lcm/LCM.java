import java.math.BigInteger;
import java.util.*;

public class LCM {
  private static BigInteger lcm(int a, int b) {

    GreatestCommonDenominator GCD = new GreatestCommonDenominator();

    return BigInteger.valueOf(a).multiply(BigInteger.valueOf(b)).divide(BigInteger.valueOf(GCD.findGCD(a, b)));

  }

  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    int a = scanner.nextInt();
    int b = scanner.nextInt();

    System.out.println(lcm(a, b));
  }
  static class GreatestCommonDenominator {

    public int findGCD(int num1, int num2) {

      int smaller, larger;
      if (num1>num2){
        smaller = num2;
        larger = num1;
      }
      else
      {
        smaller = num1;
        larger = num2;
      }
      if (smaller==0)
        return larger;

      int remainder = larger % smaller;
      return findGCD(smaller, remainder);

    }
  }
}
