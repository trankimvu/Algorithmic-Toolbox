package Week2_Assignment.starter_files.fibonacci_last_digit;

import java.util.*;

public class FibonacciLastDigit {
    private static LinkedList<Integer> lastTwoFibonacciLastDigit;
    static {
        lastTwoFibonacciLastDigit = new LinkedList<>();
        lastTwoFibonacciLastDigit.add(0);
        lastTwoFibonacciLastDigit.add(1);
    }

    public static int getFibonacciLastDigit(long n) {

        if (n<=1)
            return (int) n;

        int lastDigit = 1;

        for (int i = 2; i <= n; i++) {
            lastDigit = (lastTwoFibonacciLastDigit.getLast()+lastTwoFibonacciLastDigit.removeFirst())%10;
            lastTwoFibonacciLastDigit.add(lastDigit);
        }

        return lastDigit;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int c = getFibonacciLastDigit(n);
        System.out.println(c);
    }
}

