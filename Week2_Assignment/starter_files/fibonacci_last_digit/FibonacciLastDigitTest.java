package Week2_Assignment.starter_files.fibonacci_last_digit;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vutran on 8/22/16.
 */
public class FibonacciLastDigitTest {
    @Test
    public void getFibonacciLastDigit() throws Exception {
        System.out.println(FibonacciLastDigit.getFibonacciLastDigit(296));
        assertEquals("Input is 200", 5, FibonacciLastDigit.getFibonacciLastDigit(200));
        assertEquals("Input is 331", 9, FibonacciLastDigit.getFibonacciLastDigit(331));
        assertEquals("Input is 327305", 5, FibonacciLastDigit.getFibonacciLastDigit(327305));

    }

}