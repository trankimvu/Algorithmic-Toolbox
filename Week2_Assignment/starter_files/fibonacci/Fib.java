import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class Fib {
  private static LinkedList<Integer> lastTwoFibonacciLastDigit;
  static {
    lastTwoFibonacciLastDigit = new LinkedList<>();
    lastTwoFibonacciLastDigit.add(0);
    lastTwoFibonacciLastDigit.add(1);
  }

  public static int getFibonacciSum(long n) {

    if (n<=1)
      return (int) n;

    int lastDigit = 1;

    for (int i = 2; i <= n; i++) {
      lastDigit = (lastTwoFibonacciLastDigit.getLast()+lastTwoFibonacciLastDigit.removeFirst())%10;
      lastTwoFibonacciLastDigit.add(lastDigit);
    }

    return lastDigit;
  }

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    long n = scanner.nextLong();
    int c = getFibonacciSum(n);
    System.out.println(c);
  }
}
