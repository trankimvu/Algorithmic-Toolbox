package Week2_Assignment.starter_files.fibonacci_sum;
/**
 * Problem Introduction
 The goal in this problem is to  nd the last digit of a sum of the  rst 𝑛 Fibonacci numbers.
 Problem Description
 Task. Given an integer 𝑛,  nd the last digit of the sum 𝐹0 +𝐹1 +···+𝐹𝑛. Input Format. The input consists of a single integer 𝑛.
 Constraints. 0 ≤ 𝑛 ≤ 10^18.
 Output Format. Output the last digit of 𝐹0 + 𝐹1 + · · · + 𝐹𝑛.
 Time Limits: 1.5 second
 Memory Limit. 512Mb.
 Sample 1.
 Input:
 3
 Output:
 4
 Explanation:
 𝐹0 +𝐹1 +𝐹2 +𝐹3 =0+1+1+2=4.
 Sample 2.
 Input:
 100
 Output:
 5
 Explanation:
 The sum is equal to 927_372_692_193_078_999_175, the last digit is 5.
 *
 */

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.*;

public class FibonacciSum {
    private static  int[] lastTwoFibonacciLastDigit;
    private static int currentIndex;
    static {
        lastTwoFibonacciLastDigit = new int[2];
        lastTwoFibonacciLastDigit[0] = 0;
        lastTwoFibonacciLastDigit[1] = 1;
        currentIndex = 0;
    }


    public static long getFibonaci(long n)
    {
        double rootOf5 = Math.sqrt(5);
        double result = (Math.pow(((1+rootOf5)/2),n) - Math.pow(((1- rootOf5)/2),n))/rootOf5;
        return Math.round(result);
    }

    public static long sumFiboHiep(long n){

        if (n<=1)
            return n;

        return getFibonaci(n-1) + 1 + 2*sumFiboHiep(n-2);
    }

    public static BigInteger sumFiboVu(long n){

        if (n<=1)
            return BigInteger.valueOf(n);

        double rootOf5 = Math.sqrt(5);
        BigDecimal A = BigDecimal.valueOf((1+rootOf5) /2);
        BigDecimal B = BigDecimal.valueOf((1-rootOf5) /2);
        BigDecimal sumA = A.pow((int) (n+1)).subtract(BigDecimal.valueOf(1)).divide(A.subtract(BigDecimal.valueOf(1)));
        BigDecimal sumB = B.pow((int) (n+1)).subtract(BigDecimal.valueOf(1)).divide(B.subtract(BigDecimal.valueOf(1)));
        //927372692193078999175

        return sumA.subtract(sumB).divide(BigDecimal.valueOf(rootOf5),0, RoundingMode.HALF_UP).toBigIntegerExact();

    }


    public static int getFibonacciSum(long n) {

        if (n <= 1)
            {return 1;}

        int sumLastDigit = 1;

        for (int i = 2; i <= n; i++) {
            int lastDigitOfThisFibonacci = (lastTwoFibonacciLastDigit[0]+lastTwoFibonacciLastDigit[1])%10;
            sumLastDigit = (sumLastDigit+lastDigitOfThisFibonacci)%10;
            lastTwoFibonacciLastDigit[currentIndex] = (lastDigitOfThisFibonacci);
            currentIndex = (currentIndex==0)?1:0;
            System.out.println(i);
        }

        return sumLastDigit;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int c = getFibonacciSum(n);
        System.out.println(c);
    }
}

