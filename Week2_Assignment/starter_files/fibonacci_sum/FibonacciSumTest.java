package Week2_Assignment.starter_files.fibonacci_sum;

/**
 * Problem Introduction
 The goal in this problem is to  nd the last digit of a sum of the  rst 𝑛 Fibonacci numbers.
 Problem Description
 Task. Given an integer 𝑛,  nd the last digit of the sum 𝐹0 +𝐹1 +···+𝐹𝑛. Input Format. The input consists of a single integer 𝑛.
 Constraints. 0 ≤ 𝑛 ≤ 10^18.
 Output Format. Output the last digit of 𝐹0 + 𝐹1 + · · · + 𝐹𝑛.
 Time Limits: 1.5 second
 Memory Limit. 512Mb.
 Sample 1.
 Input:
 3
 Output:
 4
 Explanation:
 𝐹0 +𝐹1 +𝐹2 +𝐹3 =0+1+1+2=4.
 Sample 2.
 Input:
 100
 Output:
 5
 Explanation:
 The sum is equal to 927_372_692_193_078_999_175, the last digit is 5.
 *
 */

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * small manual tests,
 * tests for edge cases,
 * tests for large numbers and integer over flow,
 * big tests for time limit and memory limit checking,
 * random test generation.
 */
public class FibonacciSumTest {


    @Test
    public void sumTest() throws Exception {

//        for (int i = 1; i <=100 ; i++) {
//            System.out.println("Fibonacci number of "+i + ": " +FibonacciSum.getFibonaci(i));
//
//        }

        assertEquals("Input is 100", 5, FibonacciSum.getFibonacciSum(100));

    }

    @Test
    public void sumHiepTest() throws Exception {
        System.out.println(FibonacciSum.sumFiboHiep(Math.round(Math.pow(10, 18))));

//        assertEquals("Input is 100", 5, FibonacciSum.sumFiboHiep(100));

    }

    @Test
    public void sumVuTest() throws Exception {
//        System.out.println(FibonacciSum.sumFiboVu(100));
        //927372692193078999175

        System.out.println(FibonacciSum.sumFiboVu(Math.round(Math.pow(10, 18))));

//        assertEquals("Input is 100", 5, FibonacciSum.sumFiboHiep(100));

    }

    @Test
    public void stressTest() throws Exception {
        System.out.println("All the test case should run faster than 1.5 second");
        FibonacciSum.getFibonacciSum(new Long("832564823476"));

    }


}