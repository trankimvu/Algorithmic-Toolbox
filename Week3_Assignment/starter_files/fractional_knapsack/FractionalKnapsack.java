package Week3_Assignment.starter_files.fractional_knapsack;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

public class FractionalKnapsack {
    public static double getOptimalValue(int capacity, int[] values, int[] weights) {
        double value = 0;

        int size = values.length;
        PriorityQueue<Item> itemPriorityQueue = new PriorityQueue<>(size, Collections.reverseOrder());
        for (int i = 0; i < size; i++) {
            itemPriorityQueue.add(new Item(values[i], weights[i]));
        }

        while (capacity>0&&!itemPriorityQueue.isEmpty())
        {
            Item highestPriority = itemPriorityQueue.remove();

            if (capacity>=highestPriority.getWeight())
            {
                value+=highestPriority.getValue();
                capacity-=highestPriority.getWeight();
            }
            else
            {
                value += (((double) capacity)/highestPriority.getWeight())*highestPriority.getValue();
                capacity = 0;
            }


        }

        return value;
    }

    private static class Item implements Comparable{
        private int value;

        public int getValue() {
            return value;
        }

        public int getWeight() {
            return weight;
        }

        private int weight;
        public Double getValueWeightRatio(){
            return ((double)value/((double) weight));
        }
        public Item (int value, int weight)
        {
            this.value = value;
            this.weight = weight;
        }

        @Override
        public int compareTo(Object o) {
            Item that = (Item) o;
            return this.getValueWeightRatio().compareTo(that.getValueWeightRatio());
        }
    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int capacity = scanner.nextInt();
        int[] values = new int[n];
        int[] weights = new int[n];
        for (int i = 0; i < n; i++) {
            values[i] = scanner.nextInt();
            weights[i] = scanner.nextInt();
        }
        System.out.printf("%.4f", getOptimalValue(capacity, values, weights));
    }
} 
