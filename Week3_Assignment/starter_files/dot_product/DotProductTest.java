package Week3_Assignment.starter_files.dot_product;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by vutran on 8/25/16.
 */
public class DotProductTest {

    @Test
    public void maxProductTest(){
        Random random = new Random();
        int n  = random.nextInt(100);

        int[] a = new int[n];
        int[] b = new int[n];

        for (int i = 0; i <n ; i++) {
            a[i] = random.nextInt();
            if (random.nextInt(100) > 49)
                a[i] *= -1;
            b[i] = random.nextInt();
            if (random.nextInt(100) > 49)
                b[i] *= -1;
        }

        System.out.println(DotProduct.maxDotProduct(a, b));


    }

}