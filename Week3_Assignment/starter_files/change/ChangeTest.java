package Week3_Assignment.starter_files.change;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vutran on 8/25/16.
 */
public class ChangeTest {

    @org.junit.Test
    public void changeTest(){

        assertEquals("Change with 2 units of money", 2, Change.getChange(2));
        assertEquals("Change with 28 units of money", 6, Change.getChange(28));
    }
    @org.junit.Test
    public void changeStressTest(){

        System.out.println(Change.getChange((int) Math.pow(10, 3)));
    }


}