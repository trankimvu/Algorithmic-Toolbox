package Week3_Assignment.starter_files.covering_segments;

import java.util.*;

public class CoveringSegments {


    private static List<Integer> optimalPoints(Segment[] segments, int lowerBound, int upperBound) {
        //write your code here

        PriorityQueue<Stat> stats = new PriorityQueue<>(segments.length, Collections.reverseOrder());
        for (int i = lowerBound; i <= upperBound ; i++) {
            Set<Segment> overlapSegments = new HashSet<>();
            for (int j = 0; j < segments.length; j++) {
                if (segments[j].start<=i && i <=segments[j].end)
                {
                    overlapSegments.add(segments[j]);
                }
            }
            if (!overlapSegments.isEmpty())
            {
                stats.add(new Stat(i, overlapSegments));
            }
        }

        System.out.println(stats);

        ArrayList<Segment> visited = new ArrayList<>();
        ArrayList<Integer> points = new ArrayList<>();

        while (visited.size() != segments.length &&!stats.isEmpty())
        {
            Stat currentStat = stats.remove();
            points.add(currentStat.point);
            visited.addAll(currentStat.overlapSegments);

            PriorityQueue<Stat> newStats = new PriorityQueue<>(segments.length, Collections.reverseOrder());
            while (!stats.isEmpty())
            {
                Stat tmp = stats.remove();
                tmp.overlapSegments.removeAll(currentStat.overlapSegments);
                if (!tmp.overlapSegments.isEmpty())
                {
                    newStats.add(tmp);
                }
            }
            stats = newStats;
        }
//        Collections.sort(points);
        return points;
    }

    private static class Stat implements Comparable<Stat>{
        private int point;
        private Set<Segment> overlapSegments;


        public Stat(int point, Set<Segment> overlapSegments){
            this.point = point;
            this.overlapSegments = overlapSegments;
        }
        @Override
        public int compareTo(Stat that) {
            return ((Integer) this.overlapSegments.size()).compareTo(that.overlapSegments.size());
        }

        @Override
        public String toString(){
            return point+" -> ["+overlapSegments +"]\n";
        }

    }

    private static class Segment implements Comparable {
        private Integer start, end;

        Segment(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public int compareTo(Object o) {
            Segment that = (Segment) o;
            int thisStartCompareToThatStart = this.start.compareTo(that.start);
            if (thisStartCompareToThatStart == 0) {
                return this.end.compareTo(that.end);
            } else {
                return thisStartCompareToThatStart;
            }

        }

        @Override
        public int hashCode() {
            return start.hashCode() + end.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            Segment that = (Segment) obj;

            return (this.start==that.start && this.end==that.end);
        }

        @Override
        public String toString(){
            return "["+start+", "+end+"] ";
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Segment[] segments = new Segment[n];
        ArrayList<Segment> debug = new ArrayList<>();
        int lowerBound = Integer.MAX_VALUE, upperBound = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int start, end;
            start = scanner.nextInt();
            end = scanner.nextInt();
            segments[i] = new Segment(start, end);
            debug.add(new Segment(start, end));
            if (lowerBound>start){
                lowerBound = start;
            }
            if (upperBound< end)
            {
                upperBound = end;
            }
        }
        Collections.sort(debug);
        System.out.println(debug);
        List<Integer> points = optimalPoints(segments, lowerBound, upperBound);
        System.out.println(points.size());
        for (int point : points) {
            System.out.print(point + " ");
        }
    }
}

