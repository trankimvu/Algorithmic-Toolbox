import java.util.*;

public class LargestNumber {
    private static String largestNumber(int[] a) {
        //write your code here

        //next digit pool

        PriorityQueue<Digit> digitPriorityQueue = new PriorityQueue<>(a.length, Collections.reverseOrder());

        String result = "";
        for (int i = 0; i < a.length; i++) {
            digitPriorityQueue.add(new Digit(a[i]));
        }

        while (!digitPriorityQueue.isEmpty()) {
            result += digitPriorityQueue.remove().numberString;
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.println(largestNumber(a));
    }

    private static class Digit implements Comparable<Digit> {
        int number;
        String numberString;

        public Digit(int number) {
            this.number = number;
            this.numberString = number + "";
        }

        @Override
        public int compareTo(Digit that) {

            int minLength = (this.numberString.length() > that.numberString.length()) ? that.numberString.length() : this.numberString.length();
            int currentlyLargest= Integer.MIN_VALUE;
            //compare using the first minLength characters
            for (int i = 0; i < minLength; i++) {
                Integer value = ((Integer) Integer.parseInt(numberString.charAt(i) + ""));
                if (value>currentlyLargest)
                    {currentlyLargest = value;}
                int cmp = value.compareTo(Integer.parseInt(that.numberString.charAt(i) + ""));
                if (cmp != 0) {
                    return cmp;
                }
            }

            //compare using the next digit, whichever has a digit that is different than 0 will be the larger
            if (this.numberString.length() > that.numberString.length()) {
                for (int i = 0; i < this.numberString.length() - that.numberString.length(); i++) {

                    if (Integer.parseInt(numberString.charAt(minLength+i) + "") > Integer.parseInt(numberString.charAt(0)+""))
                        return 1;

                    if (Integer.parseInt(numberString.charAt(minLength+i) + "") < currentlyLargest)
                        return -1;
                }
                return 1;

            } else if (this.numberString.length() < that.numberString.length()) {
                for (int i = 0; i < that.numberString.length() - this.numberString.length(); i++) {
                    if (Integer.parseInt(that.numberString.charAt(minLength+i) + "") > Integer.parseInt(numberString.charAt(0)+""))
                        return -1;

                    if (Integer.parseInt(that.numberString.charAt(minLength+i) + "") < currentlyLargest)
                        return 1;
                }
                return -1;
            } else return 0;

        }
    }
}

