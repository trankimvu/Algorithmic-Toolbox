class GreatestCommonDenominator {


    public static void main(String[] args) {

        System.out.println(findGCD(357, 234));

    }

    public static int findGCD(int num1, int num2) {

        int smaller, larger;
        if (num1>num2){
            smaller = num2;
            larger = num1;
        }
        else
        {
            smaller = num1;
            larger = num2;
        }
        if (smaller==0)
            return larger;

        int remainder = larger % smaller;
        return findGCD(smaller, remainder);

    }
}
