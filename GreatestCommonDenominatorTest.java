import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vutran on 8/21/16.
 */
public class GreatestCommonDenominatorTest {

    @Test
    public void findGCDTest(){

        assertEquals("Normal case", 6, GreatestCommonDenominator.findGCD(54, 24));
        assertEquals("A number is #1", 1, GreatestCommonDenominator.findGCD(1, 24));
        assertEquals("Brutal test cases",1, GreatestCommonDenominator.findGCD(72,2345));
        assertEquals("Brutal test cases",23445, GreatestCommonDenominator.findGCD(164115,1406700));
        assertEquals("Brutal test cases",3, GreatestCommonDenominator.findGCD(339,1368));
        assertEquals("Brutal test cases",2, GreatestCommonDenominator.findGCD(434334,55534));
        assertEquals("Brutal test cases",243532, GreatestCommonDenominator.findGCD(0,243532));
        assertEquals("Brutal test cases",31415, GreatestCommonDenominator.findGCD(24440870,30315475));
//        assertEquals("Brutal test cases",564958, GreatestCommonDenominator.findGCD(366983722766,37279462087332));


    }

}