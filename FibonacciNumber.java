import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

public class FibonacciNumber {

    private static HashMap<Integer, BigInteger> fibonacciNumbers;

    static
    {
        fibonacciNumbers = new HashMap<>();
        fibonacciNumbers.put(0,new BigInteger("0"));
        fibonacciNumbers.put(1,new BigInteger("1"));
    }


    static public BigInteger fibonacciOfN(int n)
    {
        if (fibonacciNumbers.containsKey(n))
            {return fibonacciNumbers.get(n);}
            else
        {
            BigInteger tmp = fibonacciOfN(n-1).add(fibonacciOfN(n-2));
            fibonacciNumbers.put(n, tmp);
            return tmp;
        }

    }


    public static void main(String[] args) {
        System.out.println(fibonacciOfN(4));
        System.out.println(fibonacciOfN(32));
        System.out.println(fibonacciOfN(1000));

    }
}